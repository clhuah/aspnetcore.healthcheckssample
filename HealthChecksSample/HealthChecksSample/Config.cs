﻿namespace HealthChecksSample;

public static class Config
{
    public const string MpgsApiHealthCheck = "https://ap-gateway.mastercard.com/api/rest/version/72/information";
    public const string RevenueMonsterHealthCheck = "https://open.revenuemonster.my/health";
}