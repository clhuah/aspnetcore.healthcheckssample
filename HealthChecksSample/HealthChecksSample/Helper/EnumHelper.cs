﻿namespace HealthChecksSample.Helper;

public static class EnumHelper
{
    public enum CosmosDbContainerIdEnum
    {
        Container1,
        Container2
    }

    public enum CosmosDbDatabaseIdEnum
    {
        ExampleDb
    }
}