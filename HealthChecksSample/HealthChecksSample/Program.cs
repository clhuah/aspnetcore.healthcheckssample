using HealthChecks.CosmosDb;
using HealthChecks.UI.Client;
using HealthChecksSample;
using HealthChecksSample.Helper;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Diagnostics.HealthChecks;

var builder = WebApplication.CreateBuilder(args);

// Configure and set up services
ConfigureServices(builder.Services, builder.Configuration);

// Build the application
var app = builder.Build();

// Configure the application
Configure(app);

// Start the application
app.Run();

return;


// Configure and set up services
static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
{
    // Register the CosmosClient as a singleton service
    services.AddSingleton(new CosmosClient(
        configuration.GetConnectionString("CosmosDb") ??
        ThrowMissingConfigurationException("CosmosDb")));

    // Add health checks
    services.AddHealthChecks()
        // Add SQL Server health checks for AlphyMart using the connection string from configuration
        .AddSqlServer(
            configuration.GetConnectionString("SomeSqlServerConnectionString") ??
            ThrowMissingConfigurationException("SomeSqlServerConnectionString"),
            name: "SomeSqlServer", // If not specify, default name is sqlserver
            tags: new[] { "database", "db", "sql" }, // Optional
            failureStatus: HealthStatus.Degraded // Optional, default value is HealthStatus.Unhealthy
        )

        // Add a CosmosDB health check for ErrorLogger using the CosmosClient
        .AddAzureCosmosDB(
            sp =>
                sp.GetRequiredService<CosmosClient>(), _ =>
            {
                var azureCosmosDbHealthCheckOptions = new AzureCosmosDbHealthCheckOptions
                {
                    DatabaseId = EnumHelper.CosmosDbDatabaseIdEnum.ExampleDb.ToString(),
                    ContainerIds = new[]
                    {
                        EnumHelper.CosmosDbContainerIdEnum.Container1.ToString(),
                        EnumHelper.CosmosDbContainerIdEnum.Container2.ToString()
                    }
                };

                return azureCosmosDbHealthCheckOptions;
            },
            "CosmosDb",
            tags: new[] { "logger", "non-SQL" }
        )

        // Add a URL group health check for MasterCard Payment Gateway API
        .AddUrlGroup(
            new Uri(Config.MpgsApiHealthCheck, UriKind.Absolute),
            HttpMethod.Get,
            "MasterCard Payment Gateway",
            tags: new[] { "api", "mpgs", "payment-gateway" }
        )

        // Add a Redis health check using the connection string from configuration
        .AddRedis(
            configuration.GetConnectionString("RedisCache") ??
            ThrowMissingConfigurationException("RedisCache"),
            name: "Redis Cache",
            tags: new[] { "cache", "redis", "redis-cache" }
        )

        // Add a URL group health check for Revenue Monster API
        .AddUrlGroup(
            new Uri(Config.RevenueMonsterHealthCheck, UriKind.Absolute),
            HttpMethod.Get,
            "Revenue Monster API",
            tags: new[] { "api", "revenueMonster", "payment-gateway" }
        )

        // Add a SendGrid health check using the API key from configuration
        .AddSendGrid(
            configuration["SendGrid:ApiKey"] ??
            ThrowMissingConfigurationException("SendGrid:ApiKey"),
            "SendGrid",
            tags: new[] { "api", "sendGrid", "email" }
        );

    // Add Health Checks UI
    services.AddHealthChecksUI(setupSettings =>
        {
            // Configure Health Check Endpoints
            setupSettings.AddHealthCheckEndpoint("Other Services Health Check", "/health");

            // Set the maximum number of history entries per endpoint to 20.
            setupSettings.MaximumHistoryEntriesPerEndpoint(20);

            // Set the evaluation time in seconds to 60 seconds.
            setupSettings.SetEvaluationTimeInSeconds(60);
        })
        // AspNetCore.HealthChecks.UI.InMemory.Storage for in memory storage
        // To use SQL Server storage for Health Checks UI data, use AspNetCore.HealthChecks.UI.SqlServer.Storage instead.
        //
        // .AddSqlServerStorage(configuration.GetConnectionString("SqlServerConnectionString") ??
        //                     ThrowMissingConfigurationException("SqlServerConnectionString"));
        .AddInMemoryStorage();
}

// Configure the application
static void Configure(IApplicationBuilder app)
{
    // Enable HTTP Strict Transport Security (HSTS) to force HTTPS
    app.UseHsts();

    // Configure Health Check endpoint
    app.UseHealthChecks("/health",
        new HealthCheckOptions { ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse });

    app.Use(async (context, next) =>
    {
        // Add the X-Content-Type-Options header to prevent MIME sniffing
        context.Response.Headers.Add("X-Content-Type-Options", "nosniff");

        // Continue processing the request
        await next();
    });

    // Enable routing for the application
    app.UseRouting()

        // Define endpoints
        .UseEndpoints(configure =>
        {
            // Map Health Checks UI
            configure.MapHealthChecksUI(setup =>
            {
                // Optionally, add a custom stylesheet for the Health Checks UI
                // This line is used to apply a custom stylesheet; if no path is provided, the default style will be used
                setup.AddCustomStylesheet(@"wwwroot\css\HealthCheck.css");

                // Set the page title for the Health Checks UI
                setup.PageTitle = "Sample Status";

                // Set the UI path for the Health Checks UI to "/"
                // If not provided, the default UIPath is "/healthchecks-ui"
                setup.UIPath = "/";
            });
        });

    // Serve static files (e.g., CSS)
    app.UseStaticFiles();
}

// Helper function to throw a missing configuration exception
static string ThrowMissingConfigurationException(string configKey)
{
    //Throws an exception with a message indicating that the specified configuration key is missing.
    throw new InvalidOperationException($"{configKey} connection string is missing in configuration.");
}